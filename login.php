<?php
include 'header.php';
?>
<div class="col-md-4 col-md-offset-4 form-register">
  <div class="outter-form-register">
    <div class="logo-register">
      <em class="glyphicon glyphicon-user"></em>
    </div>
    <form action="proses-login.php" method="post" enctype="multipart/form-data" class="inner-register">
      <h3 class="text-center title-register"><b>Login</b></h3>
      <div class="form-group">
        <label for="username">Username *</label>
        <input type="text" class="form-control" name="username" placeholder="Username" maxlength="30" required>
      </div>
      <div class="form-group">
        <label for="password">Password *</label>
        <input type="password" class="form-control" name="password" placeholder="Password" maxlength="32" required>
      </div>
      <input type="submit" class="btn btn-block btn-info" value="Login" /><br>
      <div class="text-center ask">
        <p>Belum punya akun? Silahkan <a href="register.php"><u>Register<u></a></p>
      </div>
      <?php
      if(isset($_GET['password'])){
        if($_GET['password'] == "salah"){
          echo "<div class=\"alert alert-warning\">username atau Password tidak ditemukan!</div> <br>";
        }
      }
      ?>
    </form>
  </div>
</div>