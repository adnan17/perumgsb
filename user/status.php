<?php
session_start();
include 'header.php';
include 'koneksi.php';
if(!isset($_SESSION['username'])){
  header('location:../login.php');
}
else{
  $username= $_SESSION['username'];
}
date_default_timezone_set("Asia/Jakarta");
?>
<style type="text/css">
body{
  background-color: #d5d9e0;
}


.konfirmasi{
	margin-top: 100px;
}
.wrap {
  width: 100%;
  background-color: #d5d9e0;

}
.wrap .header {
  background-image: url(../images/home2.jpg);
  background-size: cover;
  padding: 130px;
  text-align: center;
  color: #ffffff;
}
h6 {
  font-size: 20px;
  font-weight: bold;
}
</style>
<?php
$sql = $koneksi->query("SELECT * FROM booking INNER JOIN konfirmasi ON booking.id_booking=konfirmasi.id_booking WHERE username='$username'");
?>
<div class="konfirmasi">
  <div class="col-md-12 col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h6 align="center">Status Booking</h6>
      </div>
      <div class="panel-footer">
        <table class="table table-striped table-bordered table-hover">
          <tr>
            <td>Rumah</td>
            <td>Tanggal Konfirmasi</td>
            <td>Bukti Transfer</td>
            <td>Status Booking</td>
          </tr>
            <?php
            while ($data = mysqli_fetch_array($sql)){
            echo "<tr>\n";
            echo "<td>Rumah {$data['id_rumah']}</td>\n";
            echo "<td>{$data['tanggal_konfirmasi']}</td>\n";
            echo "<td><img src=\"../images/{$data['bukti_transfer']}\" height=\"100\" width=\"100\"></td>\n";
            echo "<td>{$data['booking_status']}</td>\n";
            echo "</tr>\n";
              } ?>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
?>