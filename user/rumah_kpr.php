<?php
session_start();
include 'header.php';
include 'koneksi.php';
if(!isset($_SESSION['username'])){
  header('location:../login.php');
}
else{
  $username= $_SESSION['username'];
}


?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Griya Sutera Balaraja</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/style.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
body{
  background-size: cover;
}
</style>

  </head>
  <body>

<style type="text/css">
body{
  background-color: #d5d9e0;
}

.wrap {
  width: 100%;
  background-color: #d5d9e0;

}
.wrap .header {
  /*background-image: url(../images/pemandangan.jpg);*/
  background-image: url(../images/home2.jpg);
  background-size: cover;
  padding: 130px;
  text-align: center;
  color: #ffffff;
}
h6 {
  font-size: 20px;
  font-weight: bold;
}
</style>

<div class="wrap">
  <div class="header">
    
    <p style="font-size: 18px; font-weight: bold ; color: gold;">Booking rumah impian dimanapun dan kapanpun</p>
    <h2 style="color: gold"><?php echo "Jam : ".date('H:i'); ?></h2>

  </div>
</div>
<br>
<?php


$sql2 = $koneksi->query( "SELECT * FROM rumah where id_kategori=1");
  while($data = mysqli_fetch_array($sql2)){
    ?>
    <div class="col-sm-4 col-xs-12">
      <div class="panel panel-info text-center">
        <div class="panel-heading">
          <h6><?php echo $data['nama_rumah']; ?></h6>
        </div>
        <div class="panel-body">
          <img width="100%" height="200px" src="images/<?php echo $data['foto']; ?>">
        </div>
        <div class="panel-footer">
          <h6><?php echo "Rp. ".Rupiah( $data['harga'] ).",-";?></h6>
          <p><strong><?php echo "DP. ".Rupiah( $data['dp'] ).",-";?> </strong></p>
         
          <?php
          $datarumah = $data['id_rumah'];
          $sql1 = $koneksi->query( "SELECT * FROM booking WHERE id_rumah='$datarumah'");
          $cek = mysqli_num_rows($sql1);
          if($cek > 0){
            $status = "Terbooking";
          }else{
            $status = "Tersedia";
          }
          ?>

          <p><strong><?php echo "Status : ".$status;?> </strong></p>
          <p><?php echo $data['deskripsi']; ?></p>
          
         <a class="page-scroll" href="<?php echo "form-booking.php?id_rumah={$data['id_rumah']}"; ?>"><button class="btn btn-info" <?php if ($status=="Terbooking"){ ?> disabled <?php   } ?> >Booking</button></a> 
        </div>
      </div>
    </div>
    <?php  }?>
  </div>
</div>