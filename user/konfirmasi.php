<?php
session_start();
include 'header.php';
include 'koneksi.php';
if(!isset($_SESSION['username'])){
  header('location:../login.php');
}
else{
  $username= $_SESSION['username'];
}
date_default_timezone_set("Asia/Jakarta");
?>
<style type="text/css">
body{
  background-color: #d5d9e0;
}

p{
  font-weight: bold;
}

.konfirmasi{
	margin-top: 100px;
	margin-left: 200px;
}
.wrap {
  width: 100%;
  background-color: #d5d9e0;

}
.wrap .header {
  background-image: url(../images/pemandangan.jpg);
  background-size: cover;
  padding: 130px;
  text-align: center;
  color: #ffffff;
}
h6 {
  font-size: 20px;
  font-weight: bold;
}
</style>
<?php
$sql2 = $koneksi->query( "SELECT * FROM booking WHERE username='$username' AND booking_status='booked'");
if( mysqli_num_rows($sql2) == 0 ) {
  ?>
  <div class="konfirmasi">
  <div class="col-md-8 col-md-8">
    <div class="panel panel-info">
      <div class="panel-heading">
        <div class="panel-heading">
        <h6 align="center">Tidak ada data konfirmasi</h6>
      </div>
    </div>
  </div>
</div>
</div>
<?php
}
else{
  $data = mysqli_fetch_array($sql2);
?>
<div class="konfirmasi">
  <div class="col-md-8 col-md-8">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h6 align="center">Konfirmasi Booking</h6>
      </div>
      <div class="panel-footer">
        <p align="center">Silahkan Transfer ke no rekening 123456789 Bank BCA a/n Perum GSB</p>
        <table class="table table-striped table-bordered table-hover">
          <tr>
            <td>Id Booking</td>
            <td><?php echo $data['id_booking']; ?></td>
          </tr>
          <tr>
            <td>Rumah</td>
            <td>Rumah <?php echo $data['id_rumah']; ?></td>
          </tr>
          <tr>
            <td>Tanggal</td>
            <td><?php echo $data['tanggal']; ?></td>
          </tr>
          
          <tr>
            <td><b>Booking Masuk</b></td>
            <td><?php echo "<b>Rp. ".Rupiah( $data['harga_booking'] )." </b>"; ?></td>
          </tr>
          
        </table>
        <form action="proses-konfirmasi.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <input type="hidden" name="id_booking" value="<?php echo $data['id_booking']; ?>">
          </div>
          <div class="form-group">
            <input type="hidden" name="tanggal_konfirmasi" value="<?php echo date('Y-m-d') ?>">
          </div>
          <div class="form-group">
            <label for="gambar">Upload bukti transfer pembayaran Booking</label>
            <input type="file" name="gambar" required>
          </div>
          <button type="submit" class="btn btn-info" value="konfirmasi">Konfirmasi</button>
         <!-- <p><i>Jika belum ada konfirmasi pembayaran sampai 2 jam sebelum waktu mulai, booking akan terhapus otomatis.</i> -->
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
?>