<?php
  $conn=mysqli_connect('localhost','root','','perum'); 
  $conn=mysqli_connect('localhost','root','','perum'); 
function Rupiah( $id ) {
  return number_format( $id, 0, ", ", "." );
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Perum Griya Sutera Balaraja</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/style.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/fullcalendar.min.css" rel="stylesheet">
    <link href="css/fullcalendar.css" rel="stylesheet">
    <link href="css/fullcalendar.print.min.css" rel="stylesheet" media="print">
    <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/fullcalendar.min.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/bootstrap-datetimepicker.js"></script>
    <style>
body{
  background-image: url(../images/home2.jpg);
  background-size: cover;
}
 li.dropdown {
      display: inline-block;
    }

    .dropdown:hover .isi-dropdown {
      display: block;
    }

    .isi-dropdown a:hover {
      color: #FFFFFF !important;
    }

    .isi-dropdown {
      position: absolute;
      display: none;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
      background-color: #f9f9f9;
    }

    .isi-dropdown a {
      color: #3c3c3c !important;
    }

    .isi-dropdown a:hover {
      color: #232323 !important;
      background: #f3f3f3 !important;
    }
</style>

  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><p><b>Perum Griya Sutera Balaraja</b></p></a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <!-- <li>
              <a href="index.php"><b>Home</b></a>
            </li> -->
            <li class="dropdown"><a href="#"><b>Daftar Rumah</b></a>
            <ul class="isi-dropdown">
               <?php

             $sql2 = mysqli_query($conn,"SELECT * FROM kategori_rumah");
              
              if($sql2) {
                echo "<ul>";
                while($d=mysqli_fetch_array($sql2)) {
                  echo "<li><a href='".$d['link_menu']."'>".$d['nama_kategori']."</a></li>";
                  
                }
                echo "</ul>";
              } else {
                echo "</li>";
              }
            
            ?>
            </ul>
          </li> 
             <li>
              <a href="form-update-profile.php"><b>Update Profile</b></a>
            </li>
            <li>
              <a href="konfirmasi.php"><b>Konfirmasi</b></a>
            </li>
            <li>
              <a href="status.php"><b>Status</b></a>
            </li>
            <li>
              <a href="logout.php"><b>Logout</b></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>
</body>
</html>