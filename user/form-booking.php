<?php
session_start();
include 'header.php';
include 'koneksi.php';
$username = $_SESSION['username'];
$sql = $koneksi->query( "SELECT * FROM user WHERE username='$username'");
$data_user = mysqli_fetch_array($sql);
$id_rumah = $_GET['id_rumah'];
$sql2 = $koneksi->query( "SELECT * FROM rumah WHERE id_rumah='$id_rumah'");
$data_rumah = mysqli_fetch_array($sql2);
?>
    <div class="col-md-4 col-md-offset-4 form-register">
      <div class="outter-form-register">
        <div class="logo-register">
          <em class="glyphicon glyphicon-time"></em>
        </div>
        <form action="proses-booking.php" method="post" enctype="multipart/form-data" class="inner-register">
          <h3 class="text-center title-register"><b><?php echo $data_rumah['nama_rumah']; ?></b></h3>
          <h6 class="text-center title-register"><b>Harga Rumah : <?php echo "Rp. ".Rupiah( $data_rumah['harga'] );?></b></h6>
          <h6 class="text-center title-register"><b>Booking Rumah : <?php echo "Rp. ".Rupiah( $data_rumah['harga_booking'] );?></b></h6>

          <div class="form-group">
            <input type="hidden" class="form-control" name="username" value="<?php echo $data_user['username']; ?>">
          </div>
          <div class="form-group">
            <input type="hidden" class="form-control" name="id_rumah" value="<?php echo $data_rumah['id_rumah']; ?>">
          </div>
          <div class="form-group">
            <label for="tanggal">tanggal *</label>
            <input type="date" class="form-control" name="tanggal" required>
          </div>
       
         <div class="form-group">
            <label for="harga_booking">input booking *</label>
            <input type="number" class="form-control" name="harga_booking" placeholder="Rp." required>
          </div>
          <input type="submit" class="btn btn-block btn-info" value="Booking" />
        </form>
      </div>
    </div>
    <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/fullcalendar.min.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/bootstrap-datetimepicker.js"></script>