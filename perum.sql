-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: perum
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `id_booking` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `id_rumah` int(10) NOT NULL,
  `tanggal` varchar(12) NOT NULL,
  `booking_status` varchar(10) NOT NULL,
  `harga_booking` int(10) NOT NULL,
  PRIMARY KEY (`id_booking`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (2021050301,'bzi',1,'2021-05-04','Approved',2000000),(2021050402,'adnan',2,'2021-05-05','Approved',2000000),(2021050403,'adnan',3,'2021-05-06','Approved',70000000),(2021050504,'adnan',4,'2021-05-06','Approved',2000000);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori_rumah`
--

DROP TABLE IF EXISTS `kategori_rumah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori_rumah` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `link_menu` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori_rumah`
--

LOCK TABLES `kategori_rumah` WRITE;
/*!40000 ALTER TABLE `kategori_rumah` DISABLE KEYS */;
INSERT INTO `kategori_rumah` VALUES (2,'KPR',0,'rumah_kpr.php'),(3,'KOMERSIL',0,'rumah_komersil.php');
/*!40000 ALTER TABLE `kategori_rumah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `konfirmasi`
--

DROP TABLE IF EXISTS `konfirmasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konfirmasi` (
  `id_konfirmasi` int(20) NOT NULL AUTO_INCREMENT,
  `id_booking` int(10) NOT NULL,
  `tanggal_konfirmasi` varchar(15) NOT NULL,
  `bukti_transfer` varchar(50) NOT NULL,
  PRIMARY KEY (`id_konfirmasi`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `konfirmasi`
--

LOCK TABLES `konfirmasi` WRITE;
/*!40000 ALTER TABLE `konfirmasi` DISABLE KEYS */;
INSERT INTO `konfirmasi` VALUES (23,2021050301,'2021-05-03','burhanza.com 1935771.jpeg'),(24,2021050402,'2021-05-04','flow.jpg'),(25,2021050403,'2021-05-04','1.PNG'),(26,2021050504,'2021-05-05','AC.JPG');
/*!40000 ALTER TABLE `konfirmasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rumah`
--

DROP TABLE IF EXISTS `rumah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rumah` (
  `id_rumah` int(10) NOT NULL AUTO_INCREMENT,
  `nama_rumah` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `dp` int(11) NOT NULL,
  `harga_booking` int(11) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rumah`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rumah`
--

LOCK TABLES `rumah` WRITE;
/*!40000 ALTER TABLE `rumah` DISABLE KEYS */;
INSERT INTO `rumah` VALUES (1,'Rumah 1',150000000,'Blok B, Rumah masa depan','foto5.jpg',15000000,2000000,1),(2,'Rumah 2',130000000,'Blok C, Rumah bersama','foto2.jpg',13000000,2000000,1),(3,'Rumah 3',140000000,'Blok A, Rumah masa depan','foto1.jpg',14000000,2000000,1),(4,'Rumah 4',160000000,'Blok D, Rumah impian','foto5.jpg',16000000,2000000,1),(5,'Rumah 5',160000000,'Blok E, Rumah impian','foto5.jpg',16000000,2000000,1),(6,'Rumah 6',160000000,'Blok F, Rumah impian','foto5.jpg',16000000,2000000,1),(7,'Rumah 7',160000000,'Blok F, Rumah impian','foto5.jpg',16000000,2000000,2);
/*!40000 ALTER TABLE `rumah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `level` varchar(2) NOT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('aa','96e79218965eb72c92a549dd5a330112','Ade','08563289574','2','sumiati092020@gmail.com','aktif'),('admin','4441e5d70b3657900fa57e66db407e0b','budi','089650233327','1',NULL,'aktif'),('adnan','e10adc3949ba59abbe56e057f20f883e','Muhammad Adnan','08563289574','2',NULL,'aktif'),('adnan17','4297f44b13955235245b2497399d7a93','Muhammad Adnan','082261513755','2',NULL,'aktif'),('bzi','6180f2504dc2e62790b8a850df150cff','budi','089650233327','2',NULL,'aktif');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'perum'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-31 19:20:07
