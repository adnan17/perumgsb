<?php
include 'header.php';
include 'koneksi.php';
?>
    <div class="col-md-4 col-md-offset-4 form-register">
      <div class="outter-form-register">
        <div class="logo-register">
          <em class="glyphicon glyphicon-user"></em>
        </div>
        <form action="proses-register.php" method="post" class="inner-register">
          <h3 class="text-center title-register"><b>Registrasi</b></h3>
          <div class="form-group">
            <label for="username">Username *</label>
            <input type="text" class="form-control" name="username" placeholder="username Address" maxlength="30" required>
          </div>
          <div class="form-group">
            <label for="password">Password *</label>
            <input type="password" class="form-control" name="password" placeholder="Password minimal 6 karakter" minlength="6" maxlength="32" required>
          </div>
         <div class="form-group">
            <label for="nama">Nama *</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama minimal 2 karakter" minlength="2" maxlength="30" required>
          </div>
          <div class="form-group">
            <label for="no_hp">No. HP *</label>
            <input type="number" class="form-control" name="no_hp" placeholder="No. HP minimal 10 angka" minlength="10" maxlength="15" required>
          </div>
           <div>
           <label for="email">Email </label>
          <input class="form-control" placeholder="Masukkan Email" id="email" name="email" type="email" pattern="[^ @]*@[^ @]*"  required  />
        </div>
          <input type="submit" class="btn btn-block btn-info" value="Register"/><br>
          <div class="text-center ask">
        <p>Sudah punya akun? Silahkan <a href="login.php"><u>Login<u></a></p>
      </div>
        </form>
      </div>
    </div>