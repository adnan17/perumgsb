<?php
include 'koneksi.php';
include 'header.php';
$id_kategori = $_GET['id_kategori'];
$sql = $koneksi->query("SELECT * FROM kategori_rumah WHERE id_kategori='{$id_kategori}'");
$row = mysqli_fetch_array($sql);
?>
<!DOCTYPE html>
<html lang="id">
<head>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
 <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Form Edit Kategori</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <form action="proses-edit-kategori.php" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" value="<?php echo $row['id_kategori'];?>" name="id_kategori">
              <div class="form-group">
                <label for="nama_kategori">Nama Kategori</label>
                <input type="text" class="form-control" name="nama_kategori" value="<?php echo $row['nama_kategori'];?>" required>
              </div>
             
                <br>
                <button type="submit" class="btn btn-info" value="simpan">Update</button>
              </form>
            </div>
          </div>
        </div>