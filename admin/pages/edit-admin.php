
<?php
    include 'header.php';
	  include 'koneksi.php';

    
    $sql = $koneksi->query("SELECT * FROM user WHERE username='Admin'");
    $row = mysqli_fetch_array($sql);
?>
<!DOCTYPE html>
<html lang="id">
<head>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
 <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Update Password</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <form action="update-admin.php" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" value="<?php echo $row['username'];?>" name="username">
              <div class="form-group">
                <label for="passwordbaru">Password Baru</label>
                <input type="password" class="form-control" name="passwordbaru" placeholder="Password minimal 6 karakter" minlength="6" maxlength="32" required>
              </div>
              <div class="form-group">
                <label for="konfirmasi">Konfirmasi Password</label>
                <input type="password" class="form-control" name="konfirmasi" placeholder="Password minimal 6 karakter" minlength="6" maxlength="32" required>
              </div>
                <br>
                <button type="submit" class="btn btn-info" value="simpan">Update</button>
              </form>
            </div>
          </div>
        </div>