<?php
include 'header.php';
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="id">
<head>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Input Data Rumah</h1>
                </div> 
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="container">
            <div class="row">
                <div class="col-md-8">
              <form action="proses-tambah.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                    <label for="nama_rumah">Nama Rumah</label>
                    <input type="text" class="form-control" name="nama_rumah" placeholder="Masukkan Nama Rumah" required>
                  </div>
                  <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="number" class="form-control" name="harga" placeholder="Masukkan Harga Rumah" required>
                  </div>
                  <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control" placeholder="Masukkan Deskripsi" required></textarea>
                  <div class="form-group">
                    <label for="foto">Foto Rumah</label>
                    <input type="file" name="foto" required>
                  </div>
                  <div class="form-group">
                    <label for="dp">Dp Rumah</label>
                    <input type="number" class="form-control" name="dp" placeholder="Masukkan Harga Dp" required>
                  </div>
                  <div class="form-group">
                    <label for="harga_booking">Harga Booking</label>
                    <input type="number" class="form-control" name="harga_booking" placeholder="Masukkan Harga Booking" required>
                  </div>
                  <button type="submit" class="btn btn-info" value="simpan">Simpan</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                </form>
                </div>
            </div>
        </div>