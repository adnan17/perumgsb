<!DOCTYPE html>
<html>
<head>
    <?php
    include 'header.php';
    include 'koneksi.php';
    ?>
</head>
<body>
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Data Akun Terblokir</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Akun
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead align="center">
                            <tr align="center">
                                <th>No.</th>
                                <th>Nama Akun</th>
                                <th>Username</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql_akun = $koneksi->query( "SELECT * FROM user WHERE status='nonaktif' AND level=2" );
                            if( mysqli_num_rows( $sql_akun ) == 0 ) {
                              echo "    <tr class=\"no-data\"><td colspan=\"6\">Maaf, belum ada data akun terblokir saat ini.</td></tr>\n";   
                          } else {
                              $no = 1;
                              while( $row_akun = mysqli_fetch_array( $sql_akun ) ) {  
                                echo "      <tr class=\"data\">\n";
                                echo "    <td>{$no}</td>\n"; 
                                echo "    <td>{$row_akun['nama']}</td>\n";
                                echo "    <td>{$row_akun['username']}</td>\n";
                                echo "      <td>{$row_akun['status']}</td>\n";
                                echo "      <td>\n";
                                echo "<a href=\"proses-hapus.php?username={$row_akun['username']}\" onclick=\"javascript: return confirm('Anda yakin hapus akun ini?')\"><button type=\"submit\" class=\"btn btn-danger\">Hapus</button></a>\n";
                                echo "<a href=\"proses-unblock.php?username={$row_akun['username']}\" onclick=\"javascript: return confirm('Anda yakin unblock akun ini?')\"><button type=\"submit\" class=\"btn btn-info\">Unblock</button></a>\n";
                            echo "  </td>\n";
                            echo "  </tr>\n";
                            $no++;}
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
</body>

</html>