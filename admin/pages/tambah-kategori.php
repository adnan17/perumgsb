<?php
include 'header.php';
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="id">
<head>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Input Data Kategori</h1>
                </div> 
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="container">
            <div class="row">
                <div class="col-md-8">
              <form action="proses-tambah-kategori.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="nama_rumah">Nama Kategori</label>
                    <input type="text" class="form-control" name="nama_kategori" placeholder="Masukkan Nama Kategori" required>
                  </div>
                 
                  <button type="submit" class="btn btn-info" value="simpan">Simpan</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                </form>
                </div>
            </div>
        </div>