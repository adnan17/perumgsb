<?php
include 'koneksi.php';
date_default_timezone_set("Asia/Jakarta");
?>

<!DOCTYPE html>
<html lang="id">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dasboard Perum Griya Sutera Balaraja</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""> Administrator</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></i>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="data-booking.php"><i class="fa fa-book fa-fw"></i> Daftar Booking</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o"></i> Status Booking<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                 <li>
                                    <a href="proses.php">Proses</a>
                                </li>
                                <li>
                                    <a href="approve.php">Approve</a>
                                </li>
                            </ul>
                        </li>
                          <li>
                            <a href="data-kategori-rumah.php"><i class="fa fa-sitemap fa-fw"></i> Data Kategori</a>
                        </li>
                        <li>
                            <a href="data-rumah.php"><i class="fa fa-sitemap fa-fw"></i> Data Rumah</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user    "></i> Data Akun<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                 <li>
                                    <a href="akun-aktif.php">Akun Aktif</a>
                                </li>
                                <li>
                                    <a href="akun-terblokir.php">Akun Terblokir</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        <a href="edit-admin.php"><i class="fa fa-edit fa-fw"></i> Setting Password</a></i>
                        </li>
                       
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <!-- jQuery -->
        <script src="../vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../vendor/metisMenu/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="../vendor/raphael/raphael.min.js"></script>
        <script src="../vendor/morrisjs/morris.min.js"></script>
        <script src="../data/morris-data.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../dist/js/sb-admin-2.js"></script>

    </body>

    </html>