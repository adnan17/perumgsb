<!DOCTYPE html>
<html>
<head>
    <?php
    include 'header.php';
    include 'koneksi.php';
    function Rupiah( $id ) {
        return number_format( $id, 0, ", ", "." );
    }
    ?>
</head>
<body>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Data Booking</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Data Booking
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead align="center">
                                <tr align="center">
                                    <th width="5">No.</th>
                                    <th width="5">Username</th>
                                    <th width="5">Rumah</th>
                                    <th width="90" align="center">Tanggal</th>
                                    <th width="8">Status</th>
                                    <th width="8">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = $koneksi->query( "SELECT * FROM booking WHERE booking_status='Booked'");
                                if( mysqli_num_rows( $sql ) == 0 ) {
                                    echo "      <tr class=\"no-data\"><td colspan=\"6\">Maaf, belum ada data booking saat ini</td></tr>\n";
                                } else {
                                    $no = 1;
                                    while( $row = mysqli_fetch_array( $sql ) ) {
                                        echo "      <tr class=\"data\">\n";
                                        echo "      <td align=\"center\">{$no}</td>\n";
                                        echo "      <td align=\"center\">{$row['username']}</td>\n";
                                        echo "      <td align=\"center\">Rumah {$row['id_rumah']}</td>\n";
                                        echo "      <td align=\"center\">{$row['tanggal']}</td>\n";
                                        echo "      <td align=\"center\">{$row['booking_status']}</td>\n";
                                        echo "<td><a href=\"hapus-booking.php?id_booking={$row['id_booking']}\"  onclick=\"javascript: return confirm('Anda yakin hapus data booking ini?')\"><button type=\"submit\" class=\"btn btn-danger\">Hapus</button></a></td>\n";
                                    echo "  </tr>\n";
                                    $no++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</body>

</html>