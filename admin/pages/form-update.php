<?php
include 'koneksi.php';
include 'header.php';
$id_rumah = $_GET['id_rumah'];
$sql = $koneksi->query("SELECT * FROM rumah WHERE id_rumah='{$id_rumah}'");
$row = mysqli_fetch_array($sql);
?>
<!DOCTYPE html>
<html lang="id">
<head>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script src="bootstrap/js/jquery-3.3.1.min.js"></script>
 <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Form Edit Rumah</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <form action="proses-update.php" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" value="<?php echo $row['id_rumah'];?>" name="id_rumah">
              <div class="form-group">
                <label for="nama_rumah">Nama Rumah</label>
                <input type="text" class="form-control" name="nama_rumah" value="<?php echo $row['nama_rumah'];?>" required>
              </div>
              <div class="form-group">
                <label for="harga">Harga Rumah</label>
                <input type="number" class="form-control" name="harga" value="<?php echo $row['harga'];?>" required>
              </div>
              <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea name="deskripsi" class="form-control" required><?php echo $row['deskripsi'];?></textarea>
                <div class="form-group">
                    <label for="foto">Foto Rumah <br>
                      <?php echo "      <td align=\"center\"><img src=\"../../images/{$row['foto']}\" width=\"100\" height=\"100\"></td>\n"; ?></label>
                    <input type="file" name="foto" required>
                  </div>
                  <div class="form-group">
                <label for="dp">Dp</label>
                <input type="number" class="form-control" name="dp" value="<?php echo $row['dp'];?>" required>
              </div>
              <div class="form-group">
                <label for="harga_booking">Harga Booking</label>
                <input type="number" class="form-control" name="harga_booking" value="<?php echo $row['harga_booking'];?>" required>
              </div>
                <br>
                <button type="submit" class="btn btn-info" value="simpan">Update</button>
              </form>
            </div>
          </div>
        </div>