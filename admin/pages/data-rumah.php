<!DOCTYPE html>
<html>
<head>
    <?php
    include 'header.php';
    include 'koneksi.php';
    function Rupiah( $id ) {
        return number_format( $id, 0, ", ", "." );
    }
    ?>
</head>
<body>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Data Rumah</h1>
            </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="tambah-rumah.php"><button type="submit" class="btn btn-info">Tambah Rumah</button></a><br><br>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Data Rumah
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead align="center">
                                    <tr align="center">
                                        <th width="5">No.</th>
                                        <th width="5">Rumah</th>
                                        <th width="90" align="center">Foto</th>   
                                        <th width="5">Harga</th>                                  
                                        <th width="8">Deskripsi</th>
                                        <th width="8">DP</th>
                                        <th width="8">Harga Booking</th>
                                        <th width="8">Status</th>
                                        <th width="8">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = $koneksi->query( "SELECT * FROM rumah");
                                    if( mysqli_num_rows( $sql ) == 0 ) {
                                        echo "      <tr class=\"no-data\"><td colspan=\"6\">Maaf, belum ada data rumah saat ini</td></tr>\n";
                                    } else {
                                        $no = 1;
                                        while( $row = mysqli_fetch_array( $sql ) ) {
                                            echo "      <tr class=\"data\">\n";
                                            echo "      <td align=\"center\">{$no}</td>\n";
                                            echo "      <td align=\"center\">{$row['nama_rumah']}</td>\n";
                                            echo "      <td align=\"center\"><img src=\"../../images/{$row['foto']}\" width=\"100\" height=\"100\"></td>\n";
                                            echo "      <td>Rp. ".Rupiah( $row['harga'] )."</td>\n";
                                            echo "      <td align=\"center\">{$row['deskripsi']}</td>\n";
                                            echo "      <td align=\"center\">{$row['dp']}</td>\n";
                                            echo "      <td align=\"center\">{$row['harga_booking']}</td>\n";
                                           
                                                $datarumah = $row['id_rumah'];
                                                $sql1 = $koneksi->query( "SELECT * FROM booking WHERE id_rumah='$datarumah'");
                                                $cek = mysqli_num_rows($sql1);
                                                
                                                if($cek > 0){
                                                    $status = "Terbooking";
                                                }else{
                                                    $status = "Tersedia";
                                                }
                                            echo "      <td align=\"center\">{$status}</td>\n";    
                                            echo "      <td align=\"center\">\n";
                                           
                                               if($status=="Terbooking"){
                                                echo "<a href=\"form-update.php?id_rumah={$row['id_rumah']}\" onclick=\"javascript: return confirm('Anda yakin edit data rumah ini?')\"><button type=\"submit\" class=\"btn btn-info\" if ($status==\"Terbooking\"){ disabled } >Edit</button></a>\n";
                                               } else{
                                                echo "<a href='form-update.php?id_rumah={$row['id_rumah']}' onclick='javascript: return confirm('Anda yakin edit data rumah ini?')'><button type='submit' class='btn btn-info' >Edit</button></a>\n";
                                               }
                                            
                                            echo "<a href=\"hapus-rumah.php?id_rumah={$row['id_rumah']}\" onclick=\"javascript: return confirm('Anda yakin hapus data rumah ini?')\"><button type=\"submit\" class=\"btn btn-danger\">Hapus</button></a>\n";
                                            echo "</td>\n";
                                            echo "  </tr>\n";
                                            $no++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </body>

        </html>