<!DOCTYPE html>
<html>
<head>
    <?php
    include 'header.php';
    include'koneksi.php';
    function Rupiah( $id ) {
        return number_format( $id, 0, ", ", "." );
    }
    ?>
</head>
<body>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Status Booking</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Approved
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead align="center">
                                <tr align="center">
                                    <th width="5">No.</th>
                                    <th width="5">Username</th>
                                    <th width="5">Rumah</th>
                                    <th width="90" align="center">Tanggal</th>
                                    <th width="8">Booking</th>
                                    <th width="8">Bukti Transfer</th>
                                    <th width="8">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = $koneksi->query( "SELECT * FROM booking INNER JOIN konfirmasi ON booking.id_booking=konfirmasi.id_booking WHERE booking_status='Approved'");
                                if( mysqli_num_rows( $sql ) == 0 ) {
                                    echo "      <tr class=\"no-data\"><td colspan=\"6\">Maaf, belum ada data kostan saat ini</td></tr>\n";
                                } else {
                                    $no = 1;
                                    while( $row = mysqli_fetch_array( $sql ) ) {
                                        echo "      <tr class=\"data\">\n";
                                        echo "      <td align=\"center\">{$no}</td>\n";
                                        echo "      <td align=\"center\">{$row['username']}</td>\n";
                                        echo "      <td align=\"center\">Rumah {$row['id_rumah']}</td>\n";
                                        echo "      <td align=\"center\">{$row['tanggal']}</td>\n";
                                        echo "      <td>Rp. ".Rupiah( $row['harga_booking'] )."</td>\n";
                                        echo "      <td><a href=\"lihat.php?id_booking={$row['id_booking']}\"><button type=\"submit\" class=\"btn btn-info\">Lihat</button></a>\n";
                                        // echo "      <td align=\"center\"><img src=\"../../images/{$row['bukti_transfer']}\" width=\"100\" height=\"100\"></td>\n";
                                        echo "      <td align=\"center\">{$row['booking_status']}</td>\n";
                                    echo "  </tr>\n";
                                    $no++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</body>

</html>