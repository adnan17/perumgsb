<!DOCTYPE html>
<html>
<head>
	<?php
	include 'header.php';
	include'koneksi.php';
	date_default_timezone_set("Asia/Jakarta");
	?>
</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Booking Expired</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Data Booking
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead align="center">
								<tr align="center">
									<th width="5">No.</th>
									<th width="5">Id_booking</th>
									<th width="5">Lapangan</th>
									<th width="90" align="center">Tanggal</th>
									<th width="8" align="center">Mulai</th>
									<th width="8">Selesai</th>
									<th width="8">Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$tanggal_sekarang = date('d/m/Y');
								$exp = date('H:i', time() + (60 * 120));

								$sql = $koneksi->query( "SELECT * FROM booking WHERE tanggal='$tanggal_sekarang' AND mulai<='$exp' AND booking_status='booked'");
								if( mysqli_num_rows( $sql ) == 0 ) {
									echo "      <tr class=\"no-data\"><td colspan=\"6\">Maaf, belum ada data booking saat ini</td></tr>\n";
								} else {
									$no = 1;
									while( $row = mysqli_fetch_array( $sql ) ) {
										echo "      <tr class=\"data\">\n";
										echo "      <td align=\"center\">{$no}</td>\n";
										echo "      <td align=\"center\">{$row['id_booking']}</td>\n";
										echo "      <td align=\"center\">Lapangan {$row['id_lapangan']}</td>\n";
										echo "      <td align=\"center\">{$row['tanggal']}</td>\n";
										echo "      <td align=\"center\">{$row['mulai']}</td>\n";
										echo "      <td align=\"center\">{$row['selesai']}</td>\n";
										echo "      <td align=\"center\">{$row['booking_status']}</td>\n";
										echo "  </tr>\n";
										$no++;
									}
								}
								?>
							</tbody>
						</table>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</body>
	?>