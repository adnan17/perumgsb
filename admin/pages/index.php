<?php
session_start();
if(!isset( $_SESSION['username']) ) {
   header('location:../../login.php');
}
else{
    $username= $_SESSION['username'];
}
include 'header.php';
include 'koneksi.php';
date_default_timezone_set("Asia/Jakarta");
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Selamat Datang di Halaman Administrator</h2>
            <h5 class="page-header">Perum Griya Sutera Balaraja</h5>
        </div>
    </div>
    <?php
    $sql1 = $koneksi->query( "SELECT * FROM booking WHERE booking_status='Proses'");
    $result = mysqli_num_rows($sql1);
    if( $result === 0 ) {
        echo "<div class=\"row\">\n";
        echo "    <div class=\"col-lg-4 col-md-6\">\n";
        echo "        <div class=\"panel panel-danger\">\n";
        echo "            <div class=\"panel-heading\">\n";
        echo "                <div class=\"row\">\n";
        echo "                    <div class=\"col-xs-3\">\n";
        echo "                        <i class=\"fa fa-cc-visa fa-5x\"></i>\n";
        echo "                    </div>\n";
        echo "                    <div class=\"col-xs-9 text-right\">\n";
        echo "                        <div class=\"huge\">X</div></br>\n";
        echo "                          <div>Maaf, belum ada transfer masuk</div><br>\n";
    } 
else {
    echo "<div class=\"row\">\n";
    echo "    <div class=\"col-lg-4 col-md-6\">\n";
    echo "        <div class=\"panel panel-success\">\n";
    echo "            <div class=\"panel-heading\">\n";
    echo "                <div class=\"row\">\n";
    echo "                    <div class=\"col-xs-3\">\n";
    echo "                        <i class=\"fa fa-cc-visa fa-5x\"></i>\n";
    echo "                    </div>\n";
    echo "                    <div class=\"col-xs-9 text-right\">\n";
    echo "                        <div class=\"huge\">O</div></br>\n";
    echo "<div>Ada transfer masuk!<br><a href=\"proses.php\">Lihat</a></div>\n";

}
?>
</div>
</div>
</div>
</div>
</div>
<?php
$sql = $koneksi->query( "SELECT * FROM booking WHERE booking_status='booked'");
if( mysqli_num_rows( $sql ) === 0 ) {
    echo "<div class=\"row\">\n";
    echo "    <div class=\"col-lg-4 col-md-6\">\n";
    echo "        <div class=\"panel panel-danger\">\n";
    echo "            <div class=\"panel-heading\">\n";
    echo "                <div class=\"row\">\n";
    echo "                    <div class=\"col-xs-3\">\n";
    echo "                        <i class=\"fa fa-calendar fa-5x\"></i>\n";
    echo "                    </div>\n";
    echo "                    <div class=\"col-xs-9 text-right\">\n";
    echo "                        <div class=\"huge\">X</div></br>\n";
    echo "                          <div>Maaf, belum ada booking baru</div></br>\n";
}
else {
    echo "<div class=\"row\">\n";
    echo "    <div class=\"col-lg-4 col-md-6\">\n";
    echo "        <div class=\"panel panel-success\">\n";
    echo "            <div class=\"panel-heading\">\n";
    echo "                <div class=\"row\">\n";
    echo "                    <div class=\"col-xs-3\">\n";
    echo "                        <i class=\"fa fa-calendar fa-5x\"></i>\n";
    echo "                    </div>\n";
    echo "                    <div class=\"col-xs-9 text-right\">\n";
    echo "                        <div class=\"huge\">O</div></br>\n";
    echo "<div>Ada booking baru! <a href=\"data-booking.php\">Lihat</a></div><br>\n";

}

    ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../vendor/raphael/raphael.min.js"></script>
<script src="../vendor/morrisjs/morris.min.js"></script>
<script src="../data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
            <!-- /.row -->