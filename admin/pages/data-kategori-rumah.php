<!DOCTYPE html>
<html>
<head>
    <?php
    include 'header.php';
    include 'koneksi.php';
    function Rupiah( $id ) {
        return number_format( $id, 0, ", ", "." );
    }
    ?>
</head>
<body>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Data Kategori Rumah</h1>
            </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="tambah-kategori.php"><button type="submit" class="btn btn-info">Tambah Kategori Rumah</button></a><br><br>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Data Kategori Rumah
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead align="center">
                                    <tr align="center">
                                        <th width="4">No.</th>
                                        <th width="50" align="center">Nama Kategori</th>
                                        <th width="10">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sql = $koneksi->query( "SELECT * FROM kategori_rumah WHERE deleted=0");
                                    if( mysqli_num_rows( $sql ) == 0 ) {
                                        echo "      <tr class=\"no-data\"><td colspan=\"6\">Maaf, belum ada data rumah saat ini</td></tr>\n";
                                    } else {
                                        $no = 1;
                                        while( $row = mysqli_fetch_array( $sql ) ) {
                                            echo "      <tr class=\"data\">\n";
                                            echo "      <td align=\"left\">{$no}</td>\n";
                                            echo "      <td align=\"left\">{$row['nama_kategori']}</td>\n";
                        
                                           
                                                $datakategori = $row['id_kategori'];
                                              
                                            
                                             
                                            echo "      <td align=\"left\">\n";
                                          
                                            echo "<a href='form-update-kategori.php?id_kategori={$row['id_kategori']}' onclick='javascript: return confirm('Anda yakin edit data Kategork ini?')'><button type='submit' class='btn btn-info' >Edit</button></a>\n";
                                            
                                            
                                            echo "<a href=\"hapus-kategori.php?id_kategori={$row['id_kategori']}\" onclick=\"javascript: return confirm('Anda yakin hapus data Kategori ini?')\"><button type=\"submit\" class=\"btn btn-danger\">Hapus</button></a>\n";
                                            echo "</td>\n";
                                            echo "  </tr>\n";
                                            $no++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </body>

        </html>